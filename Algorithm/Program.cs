﻿using System;

namespace Algorithm
{
    class Program
    {
        // Префікс(суфікс) функція приймає підряток її головне завдання - заповнити масив
        static int[] PrefixFunction(string substring)
        {   
            int[] arr = new int[substring.Length];
            int f=0;

            // Перший цикл проходить по словах підрядка (весь підрядок не враховується)
            for (int i = 0; i < substring.Length; i++)
            {
                //Вкладений цикл виконує прохід по символах слова
                for (int j = 0; j < i; j++)
                {
                    //Якщо j-тий символ дорівнює останьному символу
                    if (substring[j] == substring[i])
                    {
                        //Якщо j-тий символ дорівнює останьному символу і ми проходимо не по першому символу
                        if (substring[j] == substring[i] && j > 0)
                        {
                            //Цикл, який проходить по попередніх символах, якщо після циклу f==1 то означає що
                            //префікс дорівнює суфіксу, то і-тому елементу масиву присвоюється довжина префікса
                            for (int n = 0; n < j; n++)
                            {
                                //Якщо певний попередній символ дорівнює певному попередньому символу префікса
                                if (substring[n] == substring[j + 1 + n])
                                    f = 1;
                                else
                                {
                                    f = -1;
                                    break;
                                } //Якщо в префікса останній символ дорівнює останьому символу в суфіксі, але якийсь
                                  //попередній не дорівнює відповідному попередньому суфікса, то виходимо з циклу 
                            }
                            if (f == 1)
                            {
                                arr[i] = j + 1;
                            }
                        }
                        //Якщо префікс складається з одного символуі префікс дорівнює суфіксу, то і-тому елементу масиву присвоюється довжина префікса
                        else
                            arr[i] = j + 1;
                    }
                    //Якщо префікс не дорівнює суфіксу
                    else 
                        arr[i] = 0;                
                }
            }
            return arr;
        }

        //Функція, що знаходить підрядок в рядку і повертає індекс
        static int FindSubstring(string line, string substring)
        {
            int[] arr = PrefixFunction(substring);
            int j = 0;

            //Проходимо по символах рядка
            for (int i = 0; i < line.Length; i++)
            {
                //Цикл виконується якщо ми знайшли входження в рядку першої букви підрядка,
                //але якась наступна буква рядка не відповідає рівності букві підрядка,
                //тоді ми під цю букву рядка підставляємо букву підрядка з індексом, який
                //дорівнює значенню попередньої від помилкової букви 
                while (j > 0 && substring[j] != line[i]) {
                    j = arr[j - 1]; 
                }

                //якщо відповідна буква рядка дорівнює відповдній букві підрядка
                if (substring[j] == line[i]) j++;
                
                //Коли знайшли в рядку всі входження підрядка
                if (j == substring.Length)
                {
                    return i - j + 1;
                }
            }
            return -1;
        }
        static void Main()
        {
            Console.InputEncoding = System.Text.Encoding.Unicode;
            Console.OutputEncoding = System.Text.Encoding.Unicode;

            Console.Write("Введіть рядок: ");
            string line = Console.ReadLine();

            Console.Write("Введіть підрядок: ");
            string substring = Console.ReadLine();

            int i = FindSubstring(line,substring);
            Console.WriteLine("Індекс: "+i);
        }
    }
}